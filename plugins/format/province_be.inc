<?php

/**
 * @file
 * Add province field to the belgium address format.
 */

$plugin = array(
  'title' => t('Province (Show province selector for Belgium)'),
  'format callback' => 'addressfield_be_province_generate',
  'type' => 'address',
  'weight' => -100,
);

/**
 * Format callback for province field on Belgium address.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_be_province_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'BE') {
    $format['province_block'] = array(
      '#type' => 'addressfield_container',
      '#attributes' => array(
        'class' => array(
          'addressfield-container-inline',
          'province-block',
          'country-' . $address['country'],
        ),
      ),
      '#weight' => 60,
    );

    $format['province_block']['administrative_area']['#options'] = _addressfield_be_province_list();
    $format['province_block']['administrative_area']['#title'] = t('Province');
    $format['province_block']['administrative_area']['#weight'] = 4;
    $format['province_block']['administrative_area']['#render_option_value'] = TRUE;
    $format['province_block']['administrative_area']['#empty_option'] = '--';
  }
}

/**
 * The province list in Belgium.
 */
function _addressfield_be_province_list() {
  return array(
    'VAN' => t('Antwerp'),
    'VLI' => t('Limburg'),
    'VBR' => t('Flemish Brabant'),
    'VOV' => t('East Flanders'),
    'VWV' => t('West Flanders'),
    'WHT' => t('Hainaut'),
    'WBR' => t('Walloon Brabant'),
    'WNA' => t('Namur'),
    'WLG' => t('Liège'),
    'WLX' => t('Luxembourg'),
  );
}
