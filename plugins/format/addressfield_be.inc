<?php

/**
 * @file
 * The default format for Belgium address
 */

$plugin = array(
  'title' => t('Address form (specific for Belgium)'),
  'format callback' => 'addressfield_be_format_address_generate',
  'type' => 'address',
  'weight' => -100,
);

/**
 * Format callback for Belgium address.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_be_format_address_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'BE') {

    $format['street_block']['#attributes']['class'][] = 'addressfield-container-inline';

    $format['street_block']['thoroughfare']['#title'] = t('Street');
    $format['street_block']['thoroughfare']['#attributes']['class'][] = 'street';
    $format['street_block']['thoroughfare']['#tag'] = 'span';

    $format['street_block']['premise']['#title'] = t('House number');
    $format['street_block']['premise']['#attributes']['class'][] = 'house-number';
    $format['street_block']['premise']['#size'] = 10;
    $format['street_block']['premise']['#required'] = TRUE;
    $format['street_block']['premise']['#tag'] = 'span';
    $format['street_block']['premise']['#prefix'] = ' ';
  }
}
