address_field_belgium
======================

This module is a plugin for Addressfield. It provides a user friendly Belgium
address form.


USAGE
======================

- Enable module
- Go to the addressfield field settings
- under Format handlers check "Address form (specific for Belgium)"


AUTHOR/MAINTAINER
======================

- Jeroen Tubex (JeroenT)
- Jimmy Henderickx  (StryKaizer)
